#include "mpi.h"

#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>
#include <cstdlib>
#include <fstream>

#include <chrono>
#include <thread>

using namespace std;

int w_size, w_rank, arr_offset;

vector<pair<int, int>> comparators;
//= {
//	{1, 2},
//	{0, 1},
//	{1, 2},
//	{4, 5},
//	{3, 4},
//	{4, 5},
//	{0, 3},
//	{2, 5},
//	{2, 3},
//	{1, 4},
//	{1, 2},
//	{3, 4}
//};

void merge_lines(const vector<int>& procs_up, const vector<int>& procs_down)
{
    size_t proc_count = procs_up.size() + procs_down.size();
    if (proc_count == 1) {
        return;
    }
    else if (proc_count == 2) {
        comparators.emplace_back(procs_up.front(), procs_down.front());
        return;
    }

    vector<int> procs_up_even;
    vector<int> procs_up_odd;

    for (size_t i = 0; i < procs_up.size(); i++) {
        if (i % 2) {
            procs_up_even.push_back(procs_up[i]);
        }
        else {
            procs_up_odd.push_back(procs_up[i]);
        }
    }

    vector<int> procs_down_even;
    vector<int> procs_down_odd;

    for (size_t i = 0; i < procs_down.size(); i++) {
        if (i % 2) {
            procs_down_even.push_back(procs_down[i]);
        }
        else {
            procs_down_odd.push_back(procs_down[i]);
        }
    }

    merge_lines(procs_up_odd, procs_down_odd);
    merge_lines(procs_up_even, procs_down_even);

    vector<int> result = procs_up;
    result.insert(result.end(), procs_down.begin(), procs_down.end());

    for (int i = 1; i + 1 < result.size(); i += 2) {
        comparators.emplace_back(result[i], result[i + 1]);
    }
}

void split_lines(const vector<int>& procs)
{
    if (procs.size() == 1)
        return;

    std::size_t const half_size = procs.size() / 2;
    std::vector<int> procs_up(procs.begin(), procs.begin() + half_size);
    std::vector<int> procs_down(procs.begin() + half_size, procs.end());

    split_lines(procs_up);
    split_lines(procs_down);
    merge_lines(procs_up, procs_down);
}

void build_lines() {
    std::vector<int> procs;

    procs.resize(w_size);
    for (int i = 0; i < w_size; i++)
        procs[i] = i;

    split_lines(procs);
}

void read_array(vector<int>& arr) {
    ifstream file("input.txt");
    int n;
    file >> n;
    arr_offset = w_size - n % w_size;

    arr.resize(n + arr_offset);

    for (int i = 0; i < n; i++) {
        file >> arr[i];
    }

    for (int i = 0; i < arr_offset; i++) {
        arr[n + i] = INT_MAX;
    }
}

void write_array(const vector<int>& arr, ostream& out) {
    for (size_t i = 0; i < arr.size(); i++) {
        out << arr[i];
        if (i != arr.size() - 1) {
            out << " ";
        }
    }
    out << endl;
}

void write_array_to_file(const vector<int>& arr) {
    ofstream file("output.txt");
    write_array(arr, file);
}

void write_cout(const vector<int>& arr) {
    stringstream ss;
    write_array(arr, ss);
    cout << "[" << w_rank << "]:" << ss.str();
    cout.flush();
}

void sync_write_cout(const vector<int>& arr, const string& title) {
    int sync = 0;

    if (w_rank > 0)
        MPI_Recv(
            &sync, 1, MPI_INT, w_rank - 1,
            0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    else {
        cout << title << endl;
        cout.flush();
    }

    stringstream ss;
    write_array(arr, ss);
    cout << "[" << w_rank << "]:" << ss.str();
    cout.flush();

    if (w_rank != w_size - 1)
        MPI_Send(
            &sync, 1, MPI_INT, w_rank + 1,
            0, MPI_COMM_WORLD);
    else {
        cout << endl;
        cout.flush();
    }
}

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    MPI_Comm_size(MPI_COMM_WORLD, &w_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &w_rank);

    srand(w_rank);

    vector<int> arr;
    if (w_rank == 0) {
        read_array(arr);
    }

    int arr_size = static_cast<int>(arr.size());
    MPI_Bcast(&arr_size, 1, MPI_INT, 0, MPI_COMM_WORLD);

    int block_size = arr_size / w_size;
    vector<int> work_arr;
    work_arr.resize(block_size);

    MPI_Scatter(
        arr.data(), block_size, MPI_INT,
        work_arr.data(), block_size, MPI_INT, 0, MPI_COMM_WORLD);

    sync_write_cout(work_arr, "Scattered input");
    MPI_Barrier(MPI_COMM_WORLD);

    // sort process part of array
    sort(work_arr.begin(), work_arr.end());

    // create merge lines
    build_lines();

    vector<int> other_vec(block_size);
    vector<int> temp(block_size);

    for (size_t comp_i = 0; comp_i < comparators.size(); comp_i++) {
        auto& comp = comparators[comp_i];
        if (w_rank == comp.first || w_rank == comp.second)
        {
            int other = w_rank == comp.first ? comp.second : comp.first;

            MPI_Sendrecv(
                work_arr.data(), block_size, MPI_INT, other, comp_i,
                other_vec.data(), block_size, MPI_INT, other, comp_i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            if (w_rank == comp.first) {
                for (size_t i = 0, j = 0, t_i = 0; t_i < temp.size(); t_i++) {
                    if (i < work_arr.size() && (j >= other_vec.size() || work_arr[i] < other_vec[j])) {
                        temp[t_i] = work_arr[i];
                        i++;
                    }
                    else {
                        temp[t_i] = other_vec[j];
                        j++;
                    }
                }
            }
            else
            {
                for (int i = (int)work_arr.size() - 1, j = (int)other_vec.size() - 1, t_i = (int)temp.size() - 1; t_i >= 0; t_i--) {
                    if (i >= 0 && (j < 0 || work_arr[i] > other_vec[j])) {
                        temp[t_i] = work_arr[i];
                        i--;
                    }
                    else {
                        temp[t_i] = other_vec[j];
                        j--;
                    }
                }
            }

            work_arr.swap(temp);
        }
    }

    sync_write_cout(work_arr, "Scattered result");

    MPI_Gather(
        work_arr.data(), block_size, MPI_INT,
        arr.data(), block_size, MPI_INT,
        0, MPI_COMM_WORLD);

    if (w_rank == 0) {
        arr.resize(arr.size() - arr_offset);

        cout << "Gathered input" << endl;
        write_array(arr, cout);
        write_array_to_file(arr);
    }

    MPI_Finalize();

    return 0;
}