import subprocess
from subprocess import Popen
from timeit import timeit

# cores = [1, 2, 4, 8]
# sizes = [10, 20, 50, 100]

cores = [4]
sizes = [10, 100, 1000]

for core_count in cores:
    for size in sizes:
        print(f"[cores={core_count}, size={size}] Running...")
        subprocess.call(["python", "utils/test_gen.py", str(size), "input.txt"], stdout=subprocess.DEVNULL)
        args = ['mpiexec', '-n', str(core_count), '--oversubscribe', './cmake-build-debug/batcher_sort']
        time = timeit(
            f"subprocess.call({args}, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)",
            setup="import subprocess", number=1)
        print(f"Executed in {time:8.5}s.")
        check_args = ['python', 'utils/check.py', 'input.txt', 'output.txt']
        subprocess.call(check_args)
