import argparse

parser = argparse.ArgumentParser('test generator')
parser.add_argument('input', help='Path to input file', type=str)
parser.add_argument('output', help='Path to output file', type=str)
args = parser.parse_args()

with open(args.input) as file:
    arr = file.readlines()[1].split(' ')
    arr = list(map(int, arr))

arr = sorted(arr)

with open(args.output) as file:
    res = file.readlines()[0].split(' ')
    res = list(map(int, res))

if arr == res:
    print("Passed!")
else:
    print("Failed!")
    print("Expected: %s" % ','.join(map(str, arr)))
    print("Got:      %s" % ','.join(map(str, res)))
