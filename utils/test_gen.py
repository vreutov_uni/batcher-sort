import argparse
import random

parser = argparse.ArgumentParser('test generator')
parser.add_argument('n', help='Test size', type=int)
parser.add_argument('output', help='Path to output file', type=str)
args = parser.parse_args()

n = args.n
out = args.output

with open(out, 'w') as file:
    file.write(str(n) + '\n')
    file.write(' '.join(
        str(random.randint(0, 100)) for _ in range(n)
    ))
    file.write('\n')

print('Done')
